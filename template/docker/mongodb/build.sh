docker build -t my-monogo:v1 .
# docker run -d --name my-mongo-container -p 80:80 -p 27017:27017 my-mongo

# mongosh

# 基本構文はdb.コレクション名.コマンド名(オプション)
# use study
# db.user.insert({name:'mr.a', age:10, gender:'m', hobbies:['programming']});
# db.user.insert({name:'mr.b', age:20, gender:'m', hobbies:['vi']});
# db.user.insert({name:'ms.c', age:30, gender:'f', hobbies:['programming', 'vi']});
# db.user.insert({name:'ms.d', age:40, gender:'f', hobbies:['cooking']});
