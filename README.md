---
puppeteer:
  displayHeaderFooter: true
  headerTemplate: "<div style=\"width:100%; text-align:right; border-bottom: 1pt solid #eeeeee; margin: -10px 20px 0; font-size: 8pt;\">Update:<span class='date'></span></div>"
  footerTemplate: "<div style=\"width:100%; text-align:center; border-top: 1pt solid #eeeeee; margin: 0 20px -10px 0; font-size: 8pt; \"><span style=\"width:100%; \">
  <span class='pageNumber'></span>/<span class='totalPages'></span></div>"
  margin:
    top: '15mm'
    bottom: '15mm'
    left: '10mm'
---
# 全体概要
## ユースケース
```plantuml
@startuml
actor user
 (個人IDを入力し\n個人情報を参照する) as refid

 user -> refid
@enduml
```
### API
1. 個人IDを指定し個人情報を取得する。
   /{version}/user/{id}

## 画面イメージと画面遷移
```plantuml
@startuml
(*) -r-> "
{{
salt
{^"検索画面"
  UserID    | "0001"
           | [Search] 
}
}}
" as search

search -r->[search] "
{{
salt
{^"検索結果画面"
  {#
  UserID | Name | Age 
  0001 | keita | 46
  0001 | shun  | 8
  }
  [back]
}
}}
" as result

result -r->[back] search
result -> (*)

@enduml
```

## システム構成
```plantuml
@startuml
left to right direction
rectangle USER <<Browser>> {
  actor user
}
rectangle my-service <<k8s namespace>> {
  rectangle FrontEnd {
    rectangle k8s-Ingress <<Ingress Controller>> {
      rectangle myServiceIngress <<k8s ingress>>
    }

    rectangle vuejsService <<k8s service>> {
      rectangle vuejsDeployment <<k8s deployment>> {
        collections vuejsPod <<k8s pod>> 
      }
    }
  }

  rectangle BackEnd {
    rectangle fastApiService <<k8s service>> {
      rectangle fastApiDeployment <<k8s deployment>> {
        collections fastApiPod <<k8s pod>> 
      }
    }
  }

  rectangle Database {
    rectangle mongodbService <<k8s service>> {
      rectangle mongodbStatefulSet <<k8s statefulset>> {
        database mongodbPod <<k8s pod>>
      }
    }
    rectangle mongodbPVC <<k8s persistentVolumeClaim>> {
      rectangle mongodbPV <<k8s persistentVolume>> {
        storage localDisk <<storage>>
      }
    }
    mongodbPod -> localDisk
  }
}

user --> myServiceIngress : http://localhost:80/
user --> myServiceIngress : http://localhost:80/api/{version}/xxxx
myServiceIngress --> vuejsPod : 初回\nダウンロード
myServiceIngress --> fastApiPod : ブラウザから\n直接APIアクセス
fastApiPod --> mongodbPod

@enduml
```

# コンテナ
## my-mongo(Mongodb)
### mongosh(cli)
インストールはこちら。
https://www.mongodb.com/try/download/shell

#### mongo 使い方
基本構文はdb.コレクション名.コマンド名(オプション)
##### CREATE DB
```
use study-db 
```
##### CREATE TABLE(コレクション作成)

##### INSERT
```
db.user.insert({name:'mr.a', age:10, gender:'m', hobbies:['programming']});
db.user.insert({name:'mr.b', age:20, gender:'m', hobbies:['vi']});
db.user.insert({name:'ms.c', age:30, gender:'f', hobbies:['programming', 'vi']});
db.user.insert({name:'ms.d', age:40, gender:'f', hobbies:['cooking']});
```
